DAY 05 TASKS

Day05ReadWriteFirst
-------------------

Create new project in Netbeans called Day05ReadWriteFirst.

Ask user for a line of text.

Open a text file for writing and write that line of text into the file 3 times.
The name of the file is "file.txt".
Make sure the file is closed when done writing.

Open a text file for reading this time, the same filename "file.txt".
Read all lines of text from that file and display them on the screen.


Day05GPAConversion
------------------

Create the project. In the main directory of the project create file "grades.txt" with the following or similar contents:

A
B-
C
B
F
B+
D

Every line of that file is a letter grade as per https://gradecalc.info/ca/qc/mcgill/gpa_calc.pl

Your program will read lines of this file then convert each line into 4.0 value and place it in ArrayList<Double> gradesList data structure.

Afterwards your program will:
a) display all the numerical values back to the user on a single line, comma-separated.
b) compute the average and display it
c) compute the median and display it
d) compute the standard deviation and display it

Day05NamesFromFile
---------------

In the main directory of the project create "names.txt" file with serveral names in it, one per line.

Read the file, line by line and place each name in 
ArrayList<String> nameArray = new ArrayList<>();

Display all names back to the user, comma-separated on one line.

Ask user for a search string and save it in String search variable;

String search = ...

In another loop find all names that contain the search string and display them.
Also write each of those names to "found.txt" file, one name per line.

In another loop find the longest name and display it.

Writhe the longest name to file "longestname.txt"

Example session:

Names read from file:
Jerry, Malexandra, Trixie, Tom, Barry

Enter serach string: rr

Matching name: Jerry
Matching name: Barry

Longest name is: Malexandra


Preferred solution does NOT use sorting.





