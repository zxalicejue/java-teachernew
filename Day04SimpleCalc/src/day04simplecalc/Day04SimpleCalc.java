package day04simplecalc;

import java.util.Scanner;

public class Day04SimpleCalc {

    static int getMenuChoice() {
        System.out.print("choose an option from menu:\n"
                + "1. Add\n"
                + "2. Subtract\n"
                + "3. Multiply\n"
                + "4. Divide\n"
                + "0. Exit\n"
                + "Your choice is: ");
        int choice = input.nextInt();
        input.nextLine();
        return choice;
    }

    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            int choice = getMenuChoice();
            if (choice == 0) {
                System.out.println("Good bye!");
                break; // return is 100% good as well,
                // please don't use System.exit() unless to handle a fatal error
            }
            switch (choice) {
                case 1: {
                    System.out.print("Enter 1st value: ");
                    double val1 = input.nextDouble();
                    System.out.print("Enter 2nd value: ");
                    double val2 = input.nextDouble();
                    double result = val1 + val2;
                    System.out.printf("Result is %f\n", result);
                }
                break;
                case 2: {
                    System.out.print("Enter 1st value: ");
                    double val1 = input.nextDouble();
                    System.out.print("Enter 2nd value: ");
                    double val2 = input.nextDouble();
                    double result = val1 - val2;
                    System.out.printf("Result is %f\n", result);
                }
                break;
                case 3: {
                    System.out.print("Enter 1st value: ");
                    double val1 = input.nextDouble();
                    System.out.print("Enter 2nd value: ");
                    double val2 = input.nextDouble();
                    double result = val1 * val2;
                    System.out.printf("Result is %f\n", result);
                }
                break;
                case 4: {
                    System.out.print("Enter 1st value: ");
                    double val1 = input.nextDouble();
                    System.out.print("Enter 2nd value: ");
                    double val2 = input.nextDouble();
                    double result = val1 / val2;
                    System.out.printf("Result is %f\n", result);
                }
                break;
                default:
                    System.out.println("Error: invalid choice.");
            }
        }
    }

}
