package day05treeloops;

import java.util.Scanner;

public class Day05TreeLoops {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("How big a tree do you need? ");
        int size = input.nextInt();
        for (int line = 0; line < size; line++) {
            for (int j = 0; j < size-line-1; j++) {
                System.out.print(" ");
            }
            for (int col = 0; col <= line; col++) {
                System.out.print("*");
            }
            for (int col = 0; col < line; col++) {
                System.out.print("*");
            }
            System.out.println();
        }
        
        /*
        for (int line = 0; line < 5; line++) {            
            
            System.out.println();
        }*/
    }
    
}
