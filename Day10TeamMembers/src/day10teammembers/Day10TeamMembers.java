package day10teammembers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Day10TeamMembers {

    static HashMap<String, ArrayList<String>> playersByTeams = new HashMap<>();
    
    public static void main(String[] args) {
        try (Scanner fileInput = new Scanner(new File("teams.txt"))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                String []lineSplit = line.split(":");
                // FIXME: check for length 2 exactly
                String teamName = lineSplit[0];
                String [] teamMembersArray = lineSplit[1].split(",");
                // put information into our data structure playersByTeams
                for (String memberName : teamMembersArray) {
                    if (playersByTeams.containsKey(memberName)) { // found
                        ArrayList<String> teamsList = playersByTeams.get(memberName);
                        teamsList.add(teamName); // register team on the list
                    } else { // not found
                        ArrayList<String> teamsList = new ArrayList<>();
                        teamsList.add(teamName);
                        playersByTeams.put(memberName, teamsList);
                    }
                }
            }
            //
            System.out.println("");
            for (String playerName : playersByTeams.keySet()) {
                ArrayList<String> teamsList = playersByTeams.get(playerName);
                System.out.printf("%s plays in: %s", playerName, String.join(", ", teamsList));
                System.out.println();
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }
    
}
