package day08peopleagain;

import java.util.ArrayList;

class Person {

    public Person(String name, int age) {
        setName(name);
        setAge(age);
    }

    private String name; // length 2-50
    private int age; // 1-150

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length() < 2 || name.length() > 50) {
            throw new IllegalArgumentException("Name must be between 2-50 characters long");
        }
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age < 1 || age > 150) {
            throw new IllegalArgumentException("Age must be between 1 and 150");
        }
        this.age = age;
    }
    
    @Override
    public String toString() {
        return String.format("Person: %s is %d", getName(), getAge());
    }
    
}

class Student extends Person {

    public Student(String name, int age, String program, double gpa) {
        super(name, age);        
        setProgram(program);
        setGpa(gpa);
    }

    private String program; // length 2-50
    private double gpa; // 0.0-4.0

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        if (program.length() < 2 || program.length() > 50) {
            throw new IllegalArgumentException("Program description must be 2-50 characters long");
        }
        this.program = program;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        if (gpa < 0 || gpa > 4.0) {
            throw new IllegalArgumentException("GPA must be in 0-4.0 range");
        }
        this.gpa = gpa;
    }

    @Override
    public String toString() {
        return String.format("Student: %s is %d, studying %s, gpa=%.2f",
                        getName(), getAge(), getProgram(), getGpa());
    }
    
}

class Teacher extends Person {

    public Teacher(String name, int age, String subject, int yearsOfExperience) {
        super(name, age);
        this.subject = subject;
        this.yearsOfExperience = yearsOfExperience;
    }
    
    private String subject; // length 2-50
    private int yearsOfExperience; // 0-100

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        if (subject.length() < 2 || subject.length() > 50) {
            throw new IllegalArgumentException("Subject must be 2-50 characters long");
        }
        this.subject = subject;
    }

    public int getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(int yearsOfExperience) {
        if (yearsOfExperience < 0 || yearsOfExperience > 100) {
            throw new IllegalArgumentException("Years of experience must be 0-100");
        }
        this.yearsOfExperience = yearsOfExperience;
    }
    
    @Override
    public String toString() {
        return String.format("Teacher: %s is %d, teaching %s since %d years",
                        getName(), getAge(), getSubject(), getYearsOfExperience());
    }
    
}

class SuperStudent extends Student {

    public SuperStudent(String name, int age, String program, double gpa, String superpowers) {
        super(name, age, program, gpa);
        setSuperpowers(superpowers);
    }
    
    private String superpowers;

    public String getSuperpowers() {
        return superpowers;
    }

    public void setSuperpowers(String superpowers) {
        if (superpowers.isEmpty()) {
            throw new IllegalArgumentException("Superpowers must not be empty");
        }
        this.superpowers = superpowers;
    }
    
    @Override
    public String toString() {
        return String.format("SuperStudent: %s is %d, studying %s, gpa=%.2f, powers=%s",
                        getName(), getAge(), getProgram(), getGpa(), getSuperpowers());
    }
    
}

public class Day08PeopleAgain {

    static ArrayList<Person> peopleList = new ArrayList<>();
    
    public static void main(String[] args) {
        // 1. Create 2 objects of each type, 6 in total and add them to peopleList
        peopleList.add(new Person("Jerry", 33));
        peopleList.add(new Person("Maria", 23));
        peopleList.add(new Student("Tommy", 37, "PhysEd", 3.7));
        peopleList.add(new Student("Eva", 27, "Math", 3.8));
        peopleList.add(new SuperStudent("Clark Kent", 42, "Superpowers", 4.0, "Super-everything"));
        peopleList.add(new Teacher("Jimmy", 37, "Electronics", 11));
        peopleList.add(new Teacher("Laura", 57, "Nursing", 21));
        // 2. In a loop print out all information about each object, one per line
        // suggestion - you may use 'instanceof', you may need to cast too
        
        for (Person p : peopleList) {
            System.out.println(p); // polymorphic call to toString();
            
            
        }
        
        // To get class name as a String
        // String shortClassName = p.getClass().getSimpleName();
        
        /*
        for (Person p : peopleList) {
            p.print(); // virtual call or polymorphic call
        }*/
        
        /*
        for (Person p : peopleList) {
            if (p instanceof Student) {
                Student st = (Student) p;
                System.out.printf("Student: %s is %d, studying %s, gpa=%.2f\n",
                        st.getName(), st.getAge(), st.getProgram(), st.getGpa());
            } else if (p instanceof Teacher) {
                Teacher tr = (Teacher) p;
                System.out.printf("Teacher: %s is %d, teaching %s since %d years\n",
                        tr.getName(), tr.getAge(), tr.getSubject(), tr.getYearsOfExperience());
            } else if (p instanceof Person) {
                System.out.printf("Person: %s is %d\n", p.getName(), p.getAge());
            } else {
                System.out.println("Internal error: uknown object type");
            }
        } */
        // 3. In a loop print out information only about Students from peopleList
        
    }

}
