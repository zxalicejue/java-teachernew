package day11progcalc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

class LIFOStack<T> {

    private ArrayList<T> storage = new ArrayList<>();

    void push(T item) {
        storage.add(0, item);
    }

    T pop() { // throw IndexOutOfBoundsException on empty
        if (storage.isEmpty()) {
            throw new IndexOutOfBoundsException("Stack empty");
        }
        return storage.remove(0);
    }

    T peek() { // returns top of the stack without taking it off
        if (storage.isEmpty()) {
            throw new IndexOutOfBoundsException("Stack empty");
        }
        return storage.get(0);
    }

    int size() {
        return storage.size();
    }

    @Override
    public String toString() {
        return "LIFOStack" + storage.toString();
    }

}

public class Day11ProgCalc {

    static LIFOStack<Double> stack = new LIFOStack<>();
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        try (Scanner fileInput = new Scanner(new File("program.txt"))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                // is it a floating point number? yes - push it
                try {
                    double numVal = Double.parseDouble(line);
                    stack.push(numVal);
                    continue;
                } catch (NumberFormatException ex) {
                    // do nothing, just continue below
                }
                // 
                switch (line) {
                    case "+": {
                        double v1 = stack.pop();
                        double v2 = stack.pop();
                        double result = v1 + v2;
                        stack.push(result);
                        continue;
                    }
                    case "-": {
                        double v1 = stack.pop();
                        double v2 = stack.pop();
                        double result = v1 - v2;
                        stack.push(result);
                        continue;
                    }
                    case "*": {
                        double v1 = stack.pop();
                        double v2 = stack.pop();
                        double result = v1 * v2;
                        stack.push(result);
                        continue;
                    }
                    case "/": {
                        double v1 = stack.pop();
                        double v2 = stack.pop();
                        double result = v2 / v1;
                        stack.push(result);
                        continue;
                    }
                    case "%": {
                        double v1 = stack.pop();
                        double v2 = stack.pop();
                        double result = v1 % v2;
                        stack.push(result);
                        continue;
                    }
                    case "=": {
                        double value = stack.peek();
                        System.out.printf("Top of stack: %.4f\n", value);
                        continue;
                    }
                    case "pop": {
                        stack.pop();
                        continue;
                    }
                    default:
                        // if (line.charAt(0) == '?') { }
                        if (line.matches("\\?:.*")) {
                            String message = line.split(":", 2)[1];                            
                            System.out.print(message);
                            double value = input.nextDouble();
                            stack.push(value);
                        } else {
                            System.out.println("warning: invalid line, skipping");
                        }                    
                } //end of switch
                //
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }

}
